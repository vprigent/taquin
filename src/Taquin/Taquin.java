package Taquin;

/*
 * @author Vincent Prigent
 */

import Controller.Controller;
import Model.Grid;
import Model.Model;
import View.View;

class Taquin {

    public static void main(String[] args) {
        Model m = new Model();
        Controller c = new Controller(m);
        View v = new View(c);
        m.addObserver(v);
        m.setGameGrid(new Grid());
    }
}
