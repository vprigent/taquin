package Model;

/*
 * @author Vincent Prigent
 */

import java.awt.Color;

public class Square {

	private int positionOnX;
	private int positionOnY;
	private final int value;
	private final Color color;

    public Square(int positionOnX, int positionOnY, int value) {
		this.positionOnX = positionOnX;
		this.positionOnY = positionOnY;
		this.value = value;
		this.color = new Color(255255200);
	}

	public int getPositionOnX() {
		return positionOnX;
	}

	public int getPositionOnY() {
		return positionOnY;
	}

	public int getValue() {
		return value;
	}

	public void setPositionOnX(int positionOnX) {
		this.positionOnX = positionOnX;

	}

	public void setPositionOnY(int positionOnY) {
		this.positionOnY = positionOnY;
	}

	public Color getColor() {
		return color;
	}
}
