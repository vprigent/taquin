package Model;

/*
 * @author Vincent Prigent
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class Grid {
	private int nbLines;
	private int nbColumns;
	private final ArrayList<Square> squareList;

	public Grid() {
		nbLines = 3;
		nbColumns = 3;
		squareList = new ArrayList<Square>();
		this.fillSquareList();
	}

	public int getNbLines() {
		return nbLines;
	}

	public int getNbColumns() {
		return nbColumns;
	}

	public void setNbLines(int nbLines) {
		if (nbLines >= 0) {
			this.nbLines = nbLines;
		}
		fillSquareList();
	}

	public void setNbColumns(int nbColumns) {
		if (nbColumns >= 0) {
			this.nbColumns = nbColumns;
		}
		fillSquareList();
	}

	void fillSquareList() {
		int cpt = 1;

		if (!squareList.isEmpty()) {
			squareList.removeAll(squareList);
		}

		for (int i = 0; i < nbLines; i++) {
			for (int j = 0; j < nbColumns; j++) {
				squareList.add(new Square(i, j, cpt));
				cpt++;
			}
		}
		squareList.remove(this.getSquare(nbLines - 1, nbColumns - 1));

		try {
			shuffleSquare();
		} catch (IllegalArgumentException e) {
			System.err.println("Amount of lines or columns invalid" + e);
		}
	}

	public Square getSquare(int x, int y) {
		for (Square s : squareList) {
			if (s.getPositionOnX() == x && s.getPositionOnY() == y) {
				return s;
			}
		}
		return null;
	}

	public LinkedList<Square> getMovables() {
		LinkedList<Square> movables = new LinkedList<Square>();

		int x = 0, y = 0;
		int i = 0, j = 0;
		Boolean found = false;

		while ((i < nbLines && j < nbColumns) || !found) {
			if (getSquare(i, j) == null) {
				x = i;
				y = j;
				found = true;
			}
			i++;
			if (i == nbLines) {
				j++;
				i = 0;
			}
		}

		if (getSquare(x + 1, y) != null) {
			movables.add(getSquare(x + 1, y));
		}
		if (getSquare(x, y + 1) != null) {
			movables.add(getSquare(x, y + 1));
		}
		if (getSquare(x - 1, y) != null) {
			movables.add(getSquare(x - 1, y));
		}
		if (getSquare(x, y - 1) != null) {
			movables.add(getSquare(x, y - 1));
		}

		return movables;

	}

	public void moveSquare(Square s) {
		int x = s.getPositionOnX();
		int y = s.getPositionOnY();

		if (getSquare(x + 1, y) == null && x + 1 < getNbLines()) {
			s.setPositionOnX(x + 1);
		} else if (getSquare(x, y + 1) == null && y + 1 < getNbColumns()) {
			s.setPositionOnY(y + 1);
		} else if (getSquare(x - 1, y) == null && x > 0) {
			s.setPositionOnX(x - 1);
		} else if (getSquare(x, y - 1) == null && y > 0) {
			s.setPositionOnY(y - 1);
		}
	}

	private void shuffleSquare() {
		Random random = new Random();
		LinkedList<Square> movables;
		int value = random.nextInt(1000) + 100;
		int squareValue;

		for (int i = 0; i < value; i++) {
			movables = getMovables();
			squareValue = random.nextInt(movables.size());
			moveSquare(movables.get(squareValue));
		}
	}
}
