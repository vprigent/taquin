package Model;

/*
 * @author Vincent Prigent
 */

import java.util.Observable;

public class Model extends Observable {
	private Grid gameGrid;

	public Model() {
		gameGrid = new Grid();
		setChanged();
		notifyObservers(gameGrid);
	}

	public void setGameGrid(Grid gameGrid) {
		this.gameGrid = gameGrid;
		setChanged();
		notifyObservers(gameGrid);
	}

	public Grid getGrid() {
		setChanged();
		notifyObservers(gameGrid);
		return gameGrid;
	}

	public void resetGame() {
		gameGrid.setNbColumns(3);
		gameGrid.setNbLines(3);
		setChanged();
		notifyObservers(gameGrid);
	}

}
