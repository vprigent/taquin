package View;

/*
 * @author Vincent Prigent
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import Controller.ClickSquare;
import Model.Grid;

@SuppressWarnings("serial")
class GridView extends JPanel {

    private Grid gameGrid;

	public GridView(final ClickSquare clickSquare) {
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				clickSquare.mousePressedOnSquare(arg0, getWidth(), getHeight());
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
	}

	public void paintComponent(Graphics g) {
		int beginX = this.getHeight() / 2 - gameGrid.getNbLines() * 25;
		int beginY = this.getWidth() / 2 - gameGrid.getNbColumns() * 25;
		for (int i = 0; i < gameGrid.getNbLines(); i++) {
			for (int j = 0; j < gameGrid.getNbColumns(); j++) {
				if (gameGrid.getSquare(i, j) != null) {
					g.setColor(gameGrid.getSquare(i, j).getColor());
					g.fillRect(beginY + j * 50, beginX + i * 50, 50, 50);
					g.setColor(Color.black);
					g.drawRect(beginY + j * 50, beginX + i * 50, 50, 50);
					g.drawString(Integer.toString(gameGrid.getSquare(i, j)
							.getValue()), beginY + j * 50 + 20, beginX + i * 50
							+ 30);
				} else {
					g.setColor(Color.black);
					g.drawRect(beginY + j * 50, beginX + i * 50, 50, 50);
				}
			}
		}
	}

	public void setGrid(Grid gameGrid) {
		this.gameGrid = gameGrid;
	}

}
