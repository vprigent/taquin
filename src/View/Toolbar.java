package View;

/*
 * @author Vincent Prigent
 */

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;

import javax.swing.JButton;
import javax.swing.JMenuBar;

import Controller.ToolbarController;

@SuppressWarnings("serial")
class Toolbar extends JMenuBar implements EventListener {

    public Toolbar(final ToolbarController toolbarController) {
		this.setPreferredSize(new Dimension(200, 30));

        JButton addColumn = new JButton("Column +");
        JButton removeColumn = new JButton("Column -");
        JButton addLine = new JButton("Line +");
        JButton removeLine = new JButton("Line -");
        JButton resetGame = new JButton("Reset");

		addColumn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toolbarController.addColumn();
            }
        });

		removeColumn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toolbarController.removeColumn();
            }
        });

		addLine.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toolbarController.addLine();
            }
        });

		removeLine.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toolbarController.removeLine();
            }
        });

		resetGame.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toolbarController.resetGame();
            }
        });

		this.add(addColumn);
		this.add(removeColumn);
		this.add(addLine);
		this.add(removeLine);
		this.add(resetGame);
	}
}
