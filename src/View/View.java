package View;

/*
 * @author Vincent Prigent
 */

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import Controller.Controller;
import Model.Grid;

@SuppressWarnings("serial")
public class View extends JFrame implements Observer {

    private final GridView grid;

	public View(Controller c) {
		// All this stuff needs to be changed soon, when the model is full
		this.setTitle("Taquin");
		this.setSize(640, 480);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setFocusable(false);
		this.grid = new GridView(c.getClickSquareController());
		this.add(grid, BorderLayout.CENTER);
		this.add(new Toolbar(c.getToolbarController()), BorderLayout.NORTH);
		this.setVisible(true);
	}

	@Override
	public void update(Observable o, Object arg) {
		grid.setGrid((Grid) arg);
		revalidate();
		repaint();
	}
}
