package UnitTests;

import static org.junit.Assert.*;

import org.junit.Test;

import Model.Grid;
import Model.Model;

public class ModelTest {
	
	private Model m = new Model(); 

	@Test
	public void testResetGame() {
		assertEquals("Lines equals 3 ", 3, m.getGrid().getNbLines());
		assertEquals("Columns equals 3 ", 3, m.getGrid().getNbColumns());
		
		m.getGrid().setNbColumns(5);
		assertTrue("Columns over 3", m.getGrid().getNbColumns() > 3);

		assertEquals("Lines still unchanged",3 , m.getGrid().getNbLines());
	}
	
	@Test
	public void testSetGrid() {
		Grid g = m.getGrid();
		m.getGrid().setNbColumns(6);
		assertEquals("Same grid", g, m.getGrid());
		
		g.setNbColumns(6);
		assertTrue("G is still equal with m grid", g == m.getGrid());
	}

}
