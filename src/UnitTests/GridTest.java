package UnitTests;

/*
 * @author Vincent Prigent
 */

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import Model.Grid;

public class GridTest {

	private Grid g = new Grid();

	@Test
	public void movablesSquareLenght() {
		int lenght = g.getMovables().size();

		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() + 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() + 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() + 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() - 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() + 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() - 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() - 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

		g.setNbColumns(g.getNbColumns() - 1);
		lenght = g.getMovables().size();
		assertTrue("Lenght : ", (lenght > 1 && lenght < 5));

	}

	@Test
	public void squareAccessor() {
		assertEquals("Case avec valeurs trop grande", null, g.getSquare(6, 5));
		assertEquals("Case avec valeurs trop petites", null,
				g.getSquare(-1, -5));
		assertEquals("Case avec valeurs encore improbables", null,
				g.getSquare(-1, 2));

		g.setNbColumns(g.getNbColumns() + 3);
		assertEquals(
				"Case avec valeurs trop grandes m�me apr�s agrandissement",
				null, g.getSquare(1, 6));

		assertEquals("Case avec valeurs trop petites", null, g.getSquare(2, 8));

	}

	@After
	public void resetGrid() {
		g = new Grid();
	}

}
