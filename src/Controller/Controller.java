package Controller;

/*
 * @author Vincent Prigent
 */

import Model.Model;

public class Controller {
    private final ClickSquare controllerGrid;
	private final ToolbarController toolbarController;

	public Controller(Model model) {
        this.controllerGrid = new ClickSquare(model);
		this.controllerGrid.setGrid(model.getGrid());
		this.toolbarController = new ToolbarController(model);
	}

    public ToolbarController getToolbarController() {
		return toolbarController;
	}

	public ClickSquare getClickSquareController() {
		return controllerGrid;
	}
}
