package Controller;

/*
 * @author Vincent Prigent
 */

import Model.Model;

public class ToolbarController {

    private final Model model;

    public ToolbarController(Model model) {
        this.model = model;
    }

    public void addColumn() {
        this.model.getGrid().setNbColumns(
                this.model.getGrid().getNbColumns() + 1);

    }

    public void removeColumn() {
        this.model.getGrid().setNbColumns(
                this.model.getGrid().getNbColumns() - 1);
    }

    public void addLine() {
        this.model.getGrid().setNbLines(this.model.getGrid().getNbLines() + 1);
    }

    public void removeLine() {
        this.model.getGrid().setNbLines(this.model.getGrid().getNbLines() - 1);
    }

    public void resetGame() {
        this.model.resetGame();
    }
}
