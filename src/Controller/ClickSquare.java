package Controller;

/*
 * @author Vincent Prigent
 */

import java.awt.event.MouseEvent;

import javax.swing.event.MouseInputAdapter;

import Model.Grid;
import Model.Model;
import Model.Square;

public class ClickSquare extends MouseInputAdapter {
	private Grid gameGrid = null;
	private final Model model;

	public ClickSquare(Model model) {
		this.model = model;
		gameGrid = model.getGrid();
	}

	public void mousePressedOnSquare(MouseEvent e, int width, int height) {
		int x = e.getX();
		int y = e.getY();

		for (Square s : gameGrid.getMovables()) {
			if ((x > (s.getPositionOnY() * 50 + width / 2 - gameGrid
					.getNbColumns() * 25) && x < ((s.getPositionOnY() + 1) * 50)
					+ width / 2 - gameGrid.getNbColumns() * 25)
					&& (y > (s.getPositionOnX() * 50 + height / 2 - gameGrid
							.getNbLines() * 25) && y < ((s.getPositionOnX() + 1) * 50)
							+ height / 2 - gameGrid.getNbLines() * 25)) {
				gameGrid.moveSquare(s);
				model.setGameGrid(gameGrid);
			}
		}

		gameGrid = model.getGrid();
	}

	public void setGrid(Grid grid) {
		this.gameGrid = grid;
	}

}
